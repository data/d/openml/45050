# OpenML dataset: Amazon_Electronics_Dataset

https://www.openml.org/d/45050

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Amazon Reviews data (data source) The repository has several datasets. For this case study, we are using the Electronics dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45050) of an [OpenML dataset](https://www.openml.org/d/45050). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45050/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45050/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45050/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

